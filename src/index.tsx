import { Provider } from "mobx-react";
import * as React from "react";
import * as ReactDOM from "react-dom";
import Unsplash from "unsplash-js";

import App from "./App";
import "./index.css";
import { PhotoStore } from "./photos";
// import registerServiceWorker from "./registerServiceWorker";

const unsplash = new Unsplash({
  applicationId: process.env.REACT_APP_UNSPLASH_ACCESS_KEY,
  secret: process.env.REACT_APP_UNSPLASH_SECRET_KEY
});

const photoStore = PhotoStore.create({}, { unsplash, history, addEventListener, location });

(window as any).s = photoStore;

ReactDOM.render(
  <Provider photoStore={photoStore}>
    <App />
  </Provider>,
  document.getElementById("root") as HTMLElement
);
// registerServiceWorker();
