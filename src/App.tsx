import { inject, observer } from "mobx-react";
import * as React from "react";

import "./App.css";
import { PhotoStore } from "./photos";

type AppProps = Partial<{
  photoStore: ReturnType<typeof PhotoStore.create>;
}>;

export function getKey(selector: (x?: any) => any) {
  const s = "" + selector;
  const m =
    s.match(/return\s+([A-Z$_.]+)/i) ||
    s.match(/.*?(?:=>|function.*?{(?!\s*return))\s*([A-Z$_.]+)/i);
  const name = (m && m[1]) || "";
  const keys = name.split(".");
  return keys[keys.length - 1];
}

// this issue needs to be fixed before fragment-returning function components can be properly typed:
// https://github.com/Microsoft/TypeScript/issues/21699
const App: any = (props: AppProps) => {
  const { photoStore } = props;
  if (!photoStore) {
    return "Something happened.";
  }
  const { selectedUser, photos } = photoStore;
  if (selectedUser) {
    return photos
      .filter(p => p.user === selectedUser)
      .map(p => <img key={p.id} src={p.urls.thumb || ""} />);
  }
  return photos.map(p => (
    <img key={p.id} src={p.urls.thumb || ""} onClick={() => photoStore.selectUser(p.user)} />
  ));
};

const contextPhotoStoreKey = getKey((x: AppProps) => x.photoStore);

export default inject(contextPhotoStoreKey)(observer(App));
